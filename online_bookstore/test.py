from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
import mysql.connector

mydb = mysql.connector.connect(
    host="bookstore_db",
    user="root",
    password="secret_password",
    database="online_bookstore_db"
)


class HttpGetHandler(BaseHTTPRequestHandler):


    def do_GET(self):
        mycursor = mydb.cursor()
        mycursor.execute("SELECT B.BookName, A.AuthorSurname, A.AuthorName, A.AuthorPatronymic, G.GenreName, P.PublisherName, B.BookPublishingYear, S.SeriesName FROM Books AS B JOIN Authors AS A ON B.Authors_ID = A.ID JOIN Publishers AS P ON B.Publishers_ID = P.ID JOIN Genres AS G ON B.Genres_ID = G.ID JOIN Series AS S ON B.Series_ID = S.ID")
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write('<html><head><meta charset="utf-8">'.encode())
        self.wfile.write('<title>Таблицы</title></head>'.encode())
        self.wfile.write('<body>'.encode())
        for x in mycursor:
            res_str = str(x)+'<br>'
            self.wfile.write(res_str.encode())
        self.wfile.write('</body></html>'.encode())
        mycursor.close()


def run(server_class=HTTPServer, handler_class=HttpGetHandler):
  server_address = ('', 8000)
  httpd = server_class(server_address, handler_class)
  try:
      httpd.serve_forever()
  except KeyboardInterrupt:
      httpd.server_close()
      mydb.close()

run()
